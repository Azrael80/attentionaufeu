# attentionaufeu

Projet L2-Informatique - UPJV

U.E.: Programmation Objet 1 - Java

Développer en ~60 heures 

©2020 - Mehdy Chekalil--Boulanger 

Licence: [![License: CC BY-NC-SA 4.0](https://licensebuttons.net/l/by-nc-sa/4.0/80x15.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)

## Le jeu

Une fois dans le jeu, le menu est assez intuitif, le jeu se joue à la souris, pour placer des pompiers, il suffit de cliquer sur la case du pompier, puis sur une case du jeu.

Les pompiers se déplacent tout seul vers le feu ou ils peuvent être déplacer si on clique sur la case d'un pompier et sur une direction ensuite.

Le feu se répand de façon aléatoire sur une case autour de lui et sur des intervalles aussi aléatoires.

Certaines cases ne peuvent pas reçevoir de feu:
	Les cases sur les quelles un feu a déjà été éteint
	Les cases contenant de l'eau
	Les cases dont le sol est en pierre

Certaines cases ne permettent pas les déplacements:
	Les cases en feu
	Les cases contenant de l'eau

Deux façons de gagner:
	Eteindre tous les feux
	Rendre les feu inoffensifs, c'est à dire qu'ils ne peuvent plus se répandre, ni brûlé d'arbre

Deux façons de perdre:
	Perdre tous les arbres
	Perdre tous les pompiers
	
## Fonctionnalités

* Génération de niveau aléatoire.
* Chargement de niveau, permet de jouer sur un niveau existant.
* Editeur de niveau:
    1. Création d'un niveau
    2. Sauvegarde d'un niveau
    3. Edition d'un niveau