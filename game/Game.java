package game;

import game.windows.*;
import game.items.map.*;
import game.items.map.levels.GenerateRandomLevel;
import game.items.map.levels.LoadedLevel;

import java.awt.*;
import java.io.File;
import java.util.Date;
import java.util.Random;

import javax.swing.JFileChooser;
import javax.swing.JLayeredPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Game {
    static public final int caseSD = 25; //Dimension des côtés de chaque case de la grille

    static private boolean stop; //fin du jeu

    //Compteurs
    static private int treeCount;
    static private int fireCount;
    static private int deadFireFighterCount;
    static private int points;

    //Placement de pompier
    static private int placeFireFighter;

	static private MainFrame frame;
    static private GamePane panel;
    static private StartMenuPane menuPane;
    static private JLayeredPane gameMenuPane;


    /*
        Retourne la panel principale de jeu
    */
    static public GamePane getPane() {
        return panel;
    }

    /*
        Retourne la panel du menu de jeu
    */
    static public JLayeredPane getGameMenuPane() {
        return gameMenuPane;
    }

    /*
        Retourne la frame principale de jeu
    */
    static public MainFrame getFrame() {
        return frame;
    }

    /*
        Status souris
    */
    static public void changeCursor(Cursor cursor) {
        Game.frame.setCursor(cursor);
    }

    /*
        Ajoute un arbre au compteur d'arbres
    */
    static public void addTree() {
        Game.treeCount++;
    }

    /*
        Enlève un arbre au compteur d'arbres
    */
    static public void removeTree() {
        Game.treeCount--;
    }

    /*
        Ajoute un feu au compteur de feu
    */
    static public void addFire() {
        Game.fireCount++;
    }

    /*
        Enlève un feu au compteur de feu
    */
    static public void removeFire() {
        Game.fireCount--;
    }

    /*
        Retourne le nombre de pompier morts
    */
    static public int getFireFighterDeadCount() {
        return Game.deadFireFighterCount;
    }

    /*
        Ajoute un pompier mort
    */
    static public void addFireFighterDead() {
        Game.deadFireFighterCount++;
    }

    /*
        Retourne le nombre d'arbre
    */
    static public int getTreeCount() {
        return Game.treeCount;
    }

    /*
        Retourne le nombre de feu
    */
    static public int getFireCount() {
        return Game.fireCount;
    }

    /*
        Retourne le statut du placement de pompier 
    */
    static public int getPlaceFireFighter() {
        return placeFireFighter;
    }

    /*
        Change la valeur du placement de pompier (-1: aucun, 0: normal, 1: fort)
    */
    static public void setPlaceFireFighter(int n) {
        placeFireFighter = n;
    }

    /*
        Retourne le nombre de points
    */
    static public int getPoints() {
        return Game.points;
    }

    /*
        Change les points
    */
    static public void changePoints(int points) {
        Game.points = points;
    }

    //Génère un nombre aléatoire entre min, max (max inclus)
    static public int getRandomNumberInRange(int min, int max) {
        if (min > max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        if (min == max)
            return min;

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    /*
        Utilisation des fichiers
    */
    //Sauvegarde d'une map
    static public void openSaveEditor(EditorMap map) {
        JFileChooser fc = new JFileChooser("./levels/");
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Niveau de jeu", "lvl");
        fc.setSelectedFile(new File("mon_niveau.lvl"));
        fc.setFileFilter(filter);
        int returnVal = fc.showSaveDialog(Game.getPane());

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            map.saveMap(fc.getSelectedFile());
        }
    }

    static public void openLoadMap(Map map) {
        JFileChooser fc = new JFileChooser("./levels/");
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Niveau de jeu", "lvl");
        fc.setSelectedFile(new File("mon_niveau.lvl"));
        fc.setFileFilter(filter);
        int returnVal = fc.showOpenDialog(Game.getPane());

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            map.loadMap(fc.getSelectedFile());
        }
    }

    
    /*
        Choix sur le menu
    */

    //Map générée aléatoirement
    static public void startRandomMap() {

        //Remise des compteurs à 0
        Game.treeCount = 0;
        Game.fireCount = 0;
        Game.points = 0;

        Game.frame.setVisible(false);
        Game.frame.remove(Game.menuPane);
        Game.menuPane = null;

        Game.frame.setVisible(true);
        Game.panel = new game.windows.GamePane();
        Game.panel.init(new Map(new GenerateRandomLevel(), Game.panel));
        gameMenuPane = new GameMenuPane(Game.panel);
        Game.frame.add(gameMenuPane);
        gameMenuPane.setBounds(Game.getPane().getWidth(), 0, 205, Game.getPane().getHeight());
        Game.frame.open(Game.panel);
    }

    //Map chargée avec un fichier
    static public void loadMapLevel() {

        //Remise des compteurs à 0
        Game.treeCount = 0;
        Game.fireCount = 0;
        Game.points = 0;

        JFileChooser fc = new JFileChooser("./levels/");
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Niveau de jeu", "lvl");
        fc.setSelectedFile(new File("mon_niveau.lvl"));
        fc.setFileFilter(filter);
        int returnVal = fc.showOpenDialog(Game.getPane());

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            Game.frame.setVisible(false);
            Game.frame.remove(Game.menuPane);
            Game.menuPane = null;

            Game.frame.setVisible(true);
            Game.panel = new game.windows.GamePane();
            //Création du niveau à partir du fichier selectionné
            Game.panel.init(new Map(new LoadedLevel(fc.getSelectedFile()), Game.panel));
            gameMenuPane = new GameMenuPane(Game.panel);
            Game.frame.add(gameMenuPane);
            gameMenuPane.setBounds(Game.getPane().getWidth(), 0, 205, Game.getPane().getHeight());
            Game.frame.open(Game.panel);
        }
    }

    //Editeur de map
    static public void startEditorMap() {
        Game.frame.setVisible(false);
        Game.frame.remove(Game.menuPane);
        Game.menuPane = null;

        Game.frame.setVisible(true);
        Game.panel = new game.windows.EditorPane();
        Game.panel.init(new EditorMap(Game.panel));
        EditorMenuPane p = new EditorMenuPane((EditorPane)Game.panel);
        Game.frame.add(p);
        p.setBounds(Game.getPane().getWidth(), 0, 205, Game.getPane().getHeight());
        Game.frame.open(Game.panel);
    }

    /*
        Ouvre le menu (accueil)
    */
    static public void openStartMenu() {
        Game.panel = null;
        Game.gameMenuPane = null;
        Game.menuPane = null;
        if(Game.frame != null)
        {
            Game.frame.removeAll();
            Game.frame.setVisible(false);
            Game.frame.dispose();
        }
        Game.frame = new MainFrame();
        Game.menuPane = new game.windows.StartMenuPane();
        Game.setPlaceFireFighter(-1);

        //Création de la map
        Game.frame.openMenu(Game.menuPane);
    }

    public static void loop() {
        while (!stop) {
            if(Game.panel != null)
            {
                Game.panel.onLoop(new Date());
            }
            try {
                Thread.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

	/*
		Fonction de départ pour le jeu
	*/
	static public void main(String[] args) {
		openStartMenu();
        loop();
	}
}