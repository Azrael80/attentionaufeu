package game;

import javax.swing.*;
import java.awt.*;

/*
	Classe contenant toutes les textures du jeu contenu dans le dossier images
*/

public enum Texture {

	Menu("menu.png"),
	Grass("Grass.png"),
	Grass2("Grass2.png"),
	StrongTreeDead("StrongTreeDead.png"),
	MediumTreeDead("MediumTreeDead.png"),
	WeakTreeDead("WeakTreeDead.png"),
	MediumTree("MediumTree.png"),
	StrongTree("StrongTree.png"),
	WeakTree("WeakTree.png"),
	Fire1("Fire1.png"),
	Fire2("Fire2.png"),
	Fire3("Fire3.png"),
	Fire4("Fire4.png"),
	FireFighter00("firefighter00.png"),
	FireFighter01("firefighter01.png"),
	FireFighter02("firefighter02.png"),
	FireFighter10("firefighter10.png"),
	FireFighter11("firefighter11.png"),
	FireFighter12("firefighter12.png"),
	FireFighter20("firefighter20.png"),
	FireFighter21("firefighter21.png"),
	FireFighter22("firefighter22.png"),
	FireFighter30("firefighter30.png"),
	FireFighter31("firefighter31.png"),
	FireFighter32("firefighter32.png"),
	FireFighter40("firefighter40.png"),
	FireFighter41("firefighter41.png"),
	FireFighter42("firefighter42.png"),
	FireFighter50("firefighter50.png"),
	FireFighter51("firefighter51.png"),
	FireFighter52("firefighter52.png"),
	FireFighter60("firefighter60.png"),
	FireFighter61("firefighter61.png"),
	FireFighter62("firefighter62.png"),
	FireFighter70("firefighter70.png"),
	FireFighter71("firefighter71.png"),
	FireFighter72("firefighter72.png"),
	StrongFireFighter00("strongfirefighter00.png"),
	StrongFireFighter01("strongfirefighter01.png"),
	StrongFireFighter02("strongfirefighter02.png"),
	StrongFireFighter10("strongfirefighter10.png"),
	StrongFireFighter11("strongfirefighter11.png"),
	StrongFireFighter12("strongfirefighter12.png"),
	StrongFireFighter20("strongfirefighter20.png"),
	StrongFireFighter21("strongfirefighter21.png"),
	StrongFireFighter22("strongfirefighter22.png"),
	StrongFireFighter30("strongfirefighter30.png"),
	StrongFireFighter31("strongfirefighter31.png"),
	StrongFireFighter32("strongfirefighter32.png"),
	StrongFireFighter40("strongfirefighter40.png"),
	StrongFireFighter41("strongfirefighter41.png"),
	StrongFireFighter42("strongfirefighter42.png"),
	StrongFireFighter50("strongfirefighter50.png"),
	StrongFireFighter51("strongfirefighter51.png"),
	StrongFireFighter52("strongfirefighter52.png"),
	StrongFireFighter60("strongfirefighter60.png"),
	StrongFireFighter61("strongfirefighter61.png"),
	StrongFireFighter62("strongfirefighter62.png"),
	StrongFireFighter70("strongfirefighter70.png"),
	StrongFireFighter71("strongfirefighter71.png"),
	StrongFireFighter72("strongfirefighter72.png"),
	Tombstone("tombstone.png"),
	DeadFloor("deadfloor.png"),
	StoneFloor("stonefloor.png"),
	StoneFloorOver("stonefloor2.png"),
	Water("water.png"),
	Socle("socle.png"),
	WinImage("winImage.png"),
	LoseImage("loseImage.png"),

	//Editeur
	EditorTitle("menu/editor_title.png"),
	EditorSols("menu/editor_sols.png"),
	EditorObjets("menu/editor_objets.png"),
	EditorSave("menu/editor_save.png"),
	EditorCharge("menu/editor_charge.png"),
	EditorButtonPlus("menu/button_plus.png"),
	EditorButtonLess("menu/button_less.png"),
	HomeEditorButton("menu/editor_menu.png"),
	
	//Menu jeu
	MenuBackground("menu/menu_background.png"),
	FireFighterButton("menu/firefighter_button.png"),
	BasicFireFighterFantom("BasicFireFighterFantom.png"),
	StrongFireFighterFantom("StrongFireFighterFantom.png"),
	HomeButton("menu/menu_button.png");


	private Image image;

	Texture(String name) {
		image = new ImageIcon(getClass().getResource("images/" + name)).getImage();
	}

	/*
		Récupère l'image d'une texture
	*/
	public Image getImage() {
		return image;
	}
}