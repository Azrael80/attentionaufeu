package game.items.livingitems;

import java.awt.*;
import java.util.Date;
import java.util.Hashtable;

import game.Game;
import game.Texture;
import game.items.map.cases.*;
import game.items.livingitems.firefighters.FireFighter;
import game.items.livingitems.trees.Tree;
import game.items.map.Map;
import game.windows.Animation;

public class Fire extends LivingItem implements Animation {

    private static final long serialVersionUID = 1L;
    
    // Animation du feu
    private Hashtable<Integer, Image> frames;
    private int frameCount = 0;
    private int animationCounter;
    
    private Date nextAnimation;
    private Date nextDamage;
    private Date nextHeal;

    public Fire(Case c, Map map) {
        super(c.getX(), c.getY(), 150, c, map);

        nextAnimation = nextDamage = nextHeal = new Date();

        frames = new Hashtable<Integer, Image>();
        frames.put(0, Texture.Fire1.getImage());
        frames.put(1, Texture.Fire2.getImage());
        frames.put(2, Texture.Fire3.getImage());
        frames.put(3, Texture.Fire4.getImage());

        lifeBar = new FireLifeBar(1, 15, 20, 3, this);

        //Ajout d'un feu au compteur
        Game.addFire();
    }

    public void onLoop(Date now) {
        
        if(now.compareTo(nextAnimation) >= 0) {
            animationCounter++;
            animationCounter = animationCounter % 4;
            setFrame(animationCounter);
            nextAnimation = new Date(now.getTime() + 100);
        }

        makeDamage(now);
    }

    public void makeDamage(Date now) {
        if(now.compareTo(nextDamage) >= 0)
        {
            //Il ne fait des dégats que sur les arbres présent dans sa case
            //et sur les pompiers présent sur les cases autour
            Case c = null;
            for(int i = _case.getGridX() - 1; i <= _case.getGridX() + 1; i++) {
                for(int j = _case.getGridY() - 1; j <= _case.getGridY() + 1; j++) {
                    c = map.getCase(i, j);
                    if(c != null) {
                        for(LivingItem item : c.getItems()) {
                            //Il ne fait des dégats que sur les arbres présent dans sa case
                            //et sur les pompiers présent sur les cases autour
                            if(item instanceof Tree && item.getCase() == _case || item instanceof FireFighter)
                            {
                                //si l'objet n'est pas mort, on lui enlève 5pv
                                if(item.getDead() == false)
                                    item.decreaseHealth(5);
                            }
                        }
                    }
                }
            }
            nextDamage = new Date(now.getTime() + 550);
        }

        //Le feu se regénère s'il n'est pas attaqué
        if(now.compareTo(nextHeal) >= 0) {
            if(maxHealth > health) {
                health += 5;
                if(health > maxHealth)
                    health = maxHealth;
            }
            nextHeal = new Date(now.getTime() + 1200);
        }
    }

    @Override
    public void decreaseHealth(int h) {
        super.decreaseHealth(h);
        nextHeal = new Date(new Date().getTime() + 1500);
    }

    public int getType() {
        return 20;
    }

    public void die() {
        dead = true;
        //On retire un feu au compteur
        Game.removeFire();
    }

    @Override
    public void paint(Graphics g) {
        g.drawImage(getFrame(), 0, 0, null);

        //On affiche aussi la barre avec l'eau qu'il manque
        if(!dead) {
            if(health < maxHealth) {
                lifeBar.setX(1);
                lifeBar.setY(0);
                lifeBar.paint(g);
            }
        }
    }

    //Si le feu est mort, on considère qu'il doit être supprimé
    public boolean canRemove() {
        return dead;
    }

    public int getLayerPos() {
        return (getY() * 10) + 1;
    }

    /*
        Interface animation
    */

    @Override
    public void setFrame(int n) {
        frameCount = n;
    }

    @Override
    public Image getFrame() {
        return frames.get(frameCount);
    }
}
