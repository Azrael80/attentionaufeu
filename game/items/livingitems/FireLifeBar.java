package game.items.livingitems;

import java.awt.*;

public class FireLifeBar extends LifeBar {

    public FireLifeBar(int x, int y, int width, int height, LivingItem item) {
        super(x, y, width, height, item);
    }

    public void setX(int x) {
    	this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

    //Affichage de la barre de vie, rectangle avec dégradé de couleur
    public void paint(Graphics g) {
        Color color = new Color(6, 215, 202);

		double percent = ((double)item.getMaxHealth() - (double)item.getHealth()) / (double)item.getMaxHealth();
		if(percent > 100)
			percent = 100;
    	int newWidth = (int)Math.floor((double)width * percent);

    	//Dessin du fond noir
    	g.setColor(Color.BLACK);
        g.fillRect(x - 1, y - 1, width + 2, height + 2);

        g.setColor(color);
		g.fillRect(x, y, newWidth, height);
    }
}
