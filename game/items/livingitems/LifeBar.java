package game.items.livingitems;

import java.awt.*;

public class LifeBar {

	protected int x;
	protected int y;
	protected int width;
	protected int height;
	protected LivingItem item;

    public LifeBar(int x, int y, int width, int height, LivingItem item) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.item = item;
    }

    public void setX(int x) {
    	this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

    //Affichage de la barre de vie, rectangle avec dégradé de couleur
    public void paint(Graphics g) {
        //g.drawImage(getImage(), x, y, null);
        Color color = new Color(75, 190, 5);

    	double percent = (double)item.getHealth() / (double)item.getMaxHealth();
    	int newWidth = (int)Math.floor((double)width * percent);

    	//Dessin du fond noir
    	g.setColor(Color.BLACK);
        g.fillRect(x - 1, y - 1, width + 2, height + 2);

    	if(percent < 0.25) {
    		color = new Color(255, 59, 19);
    	} else if (percent < 0.5) {
    		color = new Color(223, 145, 1);
    	} else if (percent < 0.8) {
    		color = new Color(59, 147, 6);
    	}

        g.setColor(color);
		g.fillRect(x, y, newWidth, height);
    }
}
