package game.items.livingitems;

import game.items.map.cases.*;

import java.util.Date;

import game.items.map.Map;
import game.windows.GameComponent;

abstract public class LivingItem extends GameComponent {
    private static final long serialVersionUID = 1L;
    
    protected int health;
    protected int maxHealth;
    protected boolean dead = false;
    protected LifeBar lifeBar;
    protected Case _case;
    protected Map map;

    public LivingItem(int x, int y, int health, Case c, Map map) {
        super(x, y);
        this.health = health;
        this.maxHealth = health;
        _case = c;
        this.map = map;
    }

    /*
        Getters
    */

    //Retourne la case sur la quelle se trouve l'objet
    public Case getCase() {
        return _case;
    }

    //Retourne la map
    public Map getMap() {
        return map;
    }

    /*
        Setters
    */

    abstract public void onLoop(Date now);

    //Methode, mort de l'objet.
    abstract public void die();

    //Methode, pour savoir si l'objet doit être supprimé de la map.
    abstract public boolean canRemove();

    //Retourne la position sur le layer
    abstract public int getLayerPos();

    //Retourne le type de l'instance
    abstract public int getType();

    public int getHealth() { return health; }

    public int getMaxHealth() { return maxHealth; }

    public void decreaseHealth(int h) {
        health -= h;
        checkHealth();
    }

    private void checkHealth() {
        if(health <= 0 && !dead) {
            die();
        }
    }

    public boolean getDead() { return dead; }

    public void setDead(final boolean v) { dead = v; }

}