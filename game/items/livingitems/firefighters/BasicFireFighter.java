package game.items.livingitems.firefighters;

import game.items.map.cases.*;
import game.items.map.Map;
import game.Texture;

public class BasicFireFighter extends FireFighter {

    private static final long serialVersionUID = 1L;

    public BasicFireFighter(int x, int y, Case c, Map map) {
        super(x, y, 300, c, map);
        speed = 20;
        puissance = 10;
        
        frames.put(0, Texture.FireFighter00.getImage());
        frames.put(1, Texture.FireFighter01.getImage());
        frames.put(2, Texture.FireFighter02.getImage());
        frames.put(10, Texture.FireFighter10.getImage());
        frames.put(11, Texture.FireFighter11.getImage());
        frames.put(12, Texture.FireFighter12.getImage());
        frames.put(20, Texture.FireFighter20.getImage());
        frames.put(21, Texture.FireFighter21.getImage());
        frames.put(22, Texture.FireFighter22.getImage());
        frames.put(30, Texture.FireFighter30.getImage());
        frames.put(31, Texture.FireFighter31.getImage());
        frames.put(32, Texture.FireFighter32.getImage());
        frames.put(40, Texture.FireFighter40.getImage());
        frames.put(41, Texture.FireFighter41.getImage());
        frames.put(42, Texture.FireFighter42.getImage());
        frames.put(50, Texture.FireFighter50.getImage());
        frames.put(51, Texture.FireFighter51.getImage());
        frames.put(52, Texture.FireFighter52.getImage());
        frames.put(60, Texture.FireFighter60.getImage());
        frames.put(61, Texture.FireFighter61.getImage());
        frames.put(62, Texture.FireFighter62.getImage());
        frames.put(70, Texture.FireFighter70.getImage());
        frames.put(71, Texture.FireFighter71.getImage());
        frames.put(72, Texture.FireFighter72.getImage());
        frames.put(80, Texture.Tombstone.getImage());
    }

    public int getType() {
        return 10;
    }
}
