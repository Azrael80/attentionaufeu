package game.items.livingitems.firefighters;

import game.items.map.cases.*;
import game.pathfinder.PathFinder;
import game.items.map.Map;
import game.windows.Animation;
import game.items.livingitems.LivingItem;
import game.items.livingitems.Fire;
import game.items.livingitems.LifeBar;
import game.Game;
import game.Texture;
import java.awt.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

abstract public class FireFighter extends LivingItem implements Animation {
    private static final long serialVersionUID = 1L;

    //Deplacement
    protected int direction;
    protected int stepCount;
    protected boolean onMove;
    protected int speed;
    //protected Case target;
    protected Date nextMove, nextMoveAnimation, nextFireDamage;
    protected int puissance;
    protected ArrayList<Case> path;

    //Deplacement automatique
    protected boolean isBotMove;
    protected Case nearestFireCase;
    protected Date nextTryPath;

    //Animation
    protected Hashtable<Integer, Image> frames;
    protected int frameCount;
    protected int realX, realY;

    FireFighter(int x, int y, int h, Case c, Map map) {
        super(x * 25, (y * 25 - 12), h, c, map);
        setSize(25, 30);
        direction = 0;
        //target = c;

        realX = getX();
        realY = getY();

        nextMove = nextMoveAnimation = nextFireDamage = nextTryPath = new Date();


        frames = new Hashtable<Integer, Image>();

        lifeBar = new LifeBar(1, 1, 20, 3, this);
    }

    /*
        Getters
    */

    /*
        Setters
    */

    //Prend un chemin vers la case selectionnée
    public void setTargetCase(Case _case) {
        if(this._case != _case) {
            path = new PathFinder(this, map.getGrid(), this._case, _case).getPath();
            if(path.size() > 0)
                path.remove(path.get(0));
            nextMoveAnimation = new Date();
            isBotMove = false;
        }
    }

    /*
        Methodes
    */

    /*
        Deplace le pompier automatiquement vers le feu
        le plus proche de lui (en terme de distance et non de cases à parcourir)
    */
    public void gotoNearestFire(Date now) {
        double distance = 0;
        Case c = null;
        Case nearestCase = null;
        Case fireCase = null;
        ArrayList<Case> nearestPath = null, path;

        //On vérifie que le pompier peut à nouveau chercher un chemin
        if(now.compareTo(nextTryPath) >= 0) {
            for(LivingItem f : map.getItems()) {
                if(f instanceof Fire) {
                    if(nearestCase == null || PathFinder.distance(_case, f.getCase()) < distance) {
                        for(int x = f.getCase().getGridX() - 1; x <= f.getCase().getGridX() + 1;  x++) {
                            for(int y = f.getCase().getGridY() - 1; y <= f.getCase().getGridY() + 1;  y++) {
                                c = map.getCase(x, y);
                                if(c != null && c.canWalk(this)) {
                                    path = new PathFinder(this, map.getGrid(), _case, c).getPath();
                                    //Si un chemin existe et que la case est plus proche
                                    //que celle trouvée précedement, elle devient la nouvelle case la plus proche
                                    if(path.size() > 0) {
                                        nearestCase = c;
                                        distance = PathFinder.distance(_case, f.getCase());
                                        nearestPath = path;
                                        fireCase = f.getCase();
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if(nearestCase != null) {
                this.path = nearestPath;
                if(this.path.size() > 0)
                    this.path.remove(this.path.get(0));
                this.nearestFireCase = fireCase;
                this.isBotMove = true;
            } else {
                //Si aucune chemin n'a été trouvée
                //on autorise une prochaine tentative dans 1 seconde +- 500ms
                nextTryPath = new Date(now.getTime() + 1000 + Game.getRandomNumberInRange(0, 500));
            }
        }
    }

    //Change la direction du personnage, en fonction des coordonées x,y demandées
    public void setDirection(int x, int y) {
        int num1 = getX() - x;
        int num2 = getY() - y;
        if (num1 == 0 && num2 > 0)
            direction = 10;
        else if (num1 < 0 && num2 == 0)
            direction = 20;
        else if (num1 == 0 && num2 < 0)
            direction = 0;
        else if (num1 > 0 && num2 == 0)
            direction = 30;
        else if (num1 > 0 && num2 > 0)
            direction = 60;
        else if (num1 < 0 && num2 > 0)
            direction = 70;
        else if (num1 < 0 && num2 < 0)
            direction = 50;
        else if (num1 > 0 && num2 < 0)
            direction = 40;
    }

    //Change la position du personnage
    public void move(int x, int y, Case c) {
        setDirection(x, y);

        if(c != _case) {
            _case.removeItem(this);
            _case = c;
            c.addItem(this);
        }
        realX = x;
        realY = y;
    }

    //Fonction qui permet de faire un pas (avancer de 3 pixels)
    private void makeStep(Case target) {
        Point startPoint = new Point(getX(), getY());
        Point endPoint = new Point(target.getX(), target.getY() - 12);
        int newX = startPoint.x;
        int newY = startPoint.y;
        Case _case = null;

        if (startPoint.x < endPoint.x)
        {
            newX += 3;
            if (newX > endPoint.x)
                newX = endPoint.x;
        }
        else if (startPoint.x > endPoint.x)
        {
            newX -= 3;
            if (newX < endPoint.x)
                newX = endPoint.x;
        }

        if (startPoint.y < endPoint.y)
        {
            newY += 3;
            if (newY > endPoint.y)
                newY = endPoint.y;
        }
        else if (startPoint.y > endPoint.y)
        {
            newY -= 3;
            if (newY < endPoint.y)
                newY = endPoint.y;
        }



        _case = map.getCase((int)(newX / 25.0), (int)((newY + 12)/25));

        //si la case est différente de celle actuelle,
        //on considère qu'on est sur la case cible
        if(this._case != _case)
            _case = target;

        if(_case != null && _case.canWalk(this)) {
            move(newX, newY, _case);
        } else {
            //On ne peut pas marcher sur la case du chemin.
            //Si ce n'est pas un déplacement du bot, on cherche un nouvel
            //itinéraire vers la case demandé, sinon on cherche à nouveau
            //le feu le plus proche
            if(!isBotMove) {
                setTargetCase(path.get(path.size() - 1));
            } else {
                gotoNearestFire(new Date());
            }
        }
    }

    public boolean canRemove() {
        return false;
    }

    public int getLayerPos() {
        return (getY() * 10) + 2;
    }

    public void onLoop(Date now) {
        //si le pompier est mort, les actions ne sont pas effectuées
        if(dead) return;
        Case explore = null;
        ArrayList<Case> fireCases = new ArrayList<Case>();
        Fire fire = null;
        Case target = null;

        if(path != null && path.size() > 0) {

            //Si c'est un déplacement du bot et la case
            //cible n'est plus en feu, on cherche à nouveau
            //le feu le plus proche
            if(isBotMove && !nearestFireCase.haveFire()) {
                gotoNearestFire(now);
                return;
            }

            if(now.compareTo(nextMove) >= 0) {
                target = path.get(0);
                //Si on est sur la case, on la retire
                if(target.getX() == getX() && target.getY() - 12 == getY())
                {
                    path.remove(target);
                    if(path.size() > 0)
                        target = path.get(0);
                    else 
                        target = null;
                }

                if(target != null) {
                    onMove = true;
                    nextMove = new Date(now.getTime() + 100 - speed);
                    if(now.compareTo(nextMoveAnimation) >= 0) {
                        stepCount = (stepCount == 1 ? 2 : 1);
                        nextMoveAnimation = new Date(now.getTime() + 150);
                    }
                    setFrame(direction + stepCount);
                    makeStep(target);
                }
            }
        } else if(onMove) {
            onMove = false;
            stepCount = 0;
            setFrame(direction + stepCount);
        }

        //Extinction des feu si le pompier n'est pas en déplacement (sur toutes les cases autour)
        if(!onMove && now.compareTo(nextFireDamage) >= 0) {
            for(int x = _case.getGridX() - 1; x <= _case.getGridX() + 1; x++) {
                for(int y = _case.getGridY() - 1; y <= _case.getGridY() + 1; y++) {
                    explore = map.getCase(x, y);
                    if(explore != null && explore.haveFire()) {
                        //Si il y a le feu, la case est ajoutée à la liste
                        fireCases.add(explore);
                    }
                }
            }

            if(fireCases.size() > 0) {
                for(Case c : fireCases) {
                    //On récupère le feu
                    for(LivingItem f : c.getItems()) {
                        if(f instanceof Fire)
                            fire = (Fire)f;
                    }
                    if(fire != null)
                        fire.decreaseHealth(puissance);
                }
            } else {
                //Si aucun feu n'est autour, on cherche le feu le plus proche
                gotoNearestFire(now);
            }
            nextFireDamage = new Date(now.getTime() + 800);
        }

    }

    public void die() {
        setDead(true);
        Game.changePoints(Game.getPoints() + 1500);
        Game.addFireFighterDead();
    }

    public void paint(Graphics g) {

        //si l'utilisateur a bouger, on change sa positon
        if(realX != getX() || realY != getY()) { 
            setLocation(realX, realY);
            map.changeLayer(this);
        }

        //Affichage du socle si le pompier est selectionné sur la map
        if(this == map.getSelectedFireFighter()) {
            g.drawImage(Texture.Socle.getImage(), 0, 6, null);
        }

        //Dessin du pompier
        g.drawImage(getFrame(), 0, 0, null);
        
        //On affiche aussi la barre de vie
        if(!dead) {
            if(health < maxHealth) {
                lifeBar.setX(1);
                lifeBar.setY(0);
                lifeBar.paint(g);
            }
        }
    }

    /*
        Interface animation
    */

    public void setFrame(int n) {
        frameCount = n;
    }

    public Image getFrame() {
        return frames.get(!dead ? frameCount : 80);
    }
}
