package game.items.livingitems.firefighters;

import game.items.map.cases.*;
import game.items.map.Map;
import game.Texture;

public class StrongFireFighter extends FireFighter {

    private static final long serialVersionUID = 1L;

    public StrongFireFighter(int x, int y, Case c, Map map) {
        super(x, y, 550, c, map);
        speed = 30;
        puissance = 20;
        
        frames.put(0, Texture.StrongFireFighter00.getImage());
        frames.put(1, Texture.StrongFireFighter01.getImage());
        frames.put(2, Texture.StrongFireFighter02.getImage());
        frames.put(10, Texture.StrongFireFighter10.getImage());
        frames.put(11, Texture.StrongFireFighter11.getImage());
        frames.put(12, Texture.StrongFireFighter12.getImage());
        frames.put(20, Texture.StrongFireFighter20.getImage());
        frames.put(21, Texture.StrongFireFighter21.getImage());
        frames.put(22, Texture.StrongFireFighter22.getImage());
        frames.put(30, Texture.StrongFireFighter30.getImage());
        frames.put(31, Texture.StrongFireFighter31.getImage());
        frames.put(32, Texture.StrongFireFighter32.getImage());
        frames.put(40, Texture.StrongFireFighter40.getImage());
        frames.put(41, Texture.StrongFireFighter41.getImage());
        frames.put(42, Texture.StrongFireFighter42.getImage());
        frames.put(50, Texture.StrongFireFighter50.getImage());
        frames.put(51, Texture.StrongFireFighter51.getImage());
        frames.put(52, Texture.StrongFireFighter52.getImage());
        frames.put(60, Texture.StrongFireFighter60.getImage());
        frames.put(61, Texture.StrongFireFighter61.getImage());
        frames.put(62, Texture.StrongFireFighter62.getImage());
        frames.put(70, Texture.StrongFireFighter70.getImage());
        frames.put(71, Texture.StrongFireFighter71.getImage());
        frames.put(72, Texture.StrongFireFighter72.getImage());
        frames.put(80, Texture.Tombstone.getImage());
    }

    public int getType() {
        return 11;
    }
}
