package game.items.livingitems.trees;

import game.items.map.cases.*;
import game.items.map.Map;
import game.Texture;

public class MediumTree extends Tree {

    private static final long serialVersionUID = 1L;

    public MediumTree(int x, int y, Case c, Map map) {
        super(x, y, 180, c, map);
        this.points = 100;

        /*
            Images pour l'annimation
            0: arbre vivant
            1: arbre mort
        */
        frames.put(0, Texture.MediumTree.getImage());
        frames.put(1, Texture.MediumTreeDead.getImage());
    }

    public int getType() {
        return 1;
    }

    public void die() {
		  super.die();
    }

}
