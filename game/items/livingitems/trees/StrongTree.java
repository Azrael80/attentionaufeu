package game.items.livingitems.trees;

import game.items.map.cases.*;
import game.items.map.Map;
import game.Texture;

public class StrongTree extends Tree {

    private static final long serialVersionUID = 1L;

    public StrongTree(int x, int y, Case c, Map map) {
        super(x, y, 250, c, map);
        this.points = 200;

        /*
            Images pour l'annimation
            0: arbre vivant
            1: arbre mort
        */
        frames.put(0, Texture.StrongTree.getImage());
        frames.put(1, Texture.StrongTreeDead.getImage());
        
    }

    public int getType() {
        return 2;
    }

    public void die() {
        super.die();
    }
}