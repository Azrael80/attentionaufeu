package game.items.livingitems.trees;

import game.items.map.cases.*;
import game.items.map.Map;
import game.windows.Animation;
import game.items.livingitems.LivingItem;
import game.items.livingitems.LifeBar;
import game.Game;
import java.awt.*;
import java.util.Date;
import java.util.Hashtable;

abstract public class Tree extends LivingItem implements Animation {

    private static final long serialVersionUID = 1L;

    //Animation
    protected Hashtable<Integer, Image> frames;

    protected int points;

    Tree(final int x, final int y, final int h, final Case c, final Map map) {
        super(x, y, h, c, map);

        frames = new Hashtable<Integer, Image>();

        //Création de la barre de vie d'une hauteur de 5px et de largeur 25px
        this.lifeBar = new LifeBar(1, 1, 20, 3, this);

        //Ajout d'un arbre au compteur
        Game.addTree();
    }

    public void onLoop(Date now) {

    }

    public void die() {
		setDead(true);
		Game.removeTree();
		Game.changePoints(Game.getPoints() + points);
    }

    public void paint(final Graphics g) {
        g.drawImage(getFrame(), 0, 0, null);

        //On affiche aussi la barre de vie
        if(!dead && health < maxHealth) {
            lifeBar.paint(g);
        }
    }

    public boolean canRemove() {
        return false;
    }

    public int getLayerPos() {
        return getY() * 10;
    }

    /*
        Interface animation
    */

    public void setFrame(int n) {
        
    }

    public Image getFrame() {
        return frames.get(dead ? 1 : 0);
    }
}
