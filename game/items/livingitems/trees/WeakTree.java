package game.items.livingitems.trees;

import game.items.map.cases.*;
import game.items.map.Map;
import game.Texture;

public class WeakTree extends Tree {

    private static final long serialVersionUID = 1L;

    public WeakTree(int x, int y, Case c, Map map) {
        super(x, y, 100, c, map);
        this.points = 70;

        /*
            Images pour l'annimation
            0: arbre vivant
            1: arbre mort
        */
        frames.put(0, Texture.WeakTree.getImage());
        frames.put(1, Texture.WeakTreeDead.getImage());
    }

    public int getType() {
        return 0;
    }

    public void die() {
    	super.die();
    }
}
