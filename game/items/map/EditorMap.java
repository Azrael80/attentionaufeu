package game.items.map;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.locks.ReentrantLock;
import game.Game;
import game.Texture;
import game.items.livingitems.Fire;
import game.items.livingitems.LivingItem;
import game.items.livingitems.trees.*;
import game.items.map.levels.EditorLevel;
import game.windows.EditorPane;
import game.windows.GamePane;
import game.items.map.cases.*;

public class EditorMap extends Map  {

    public EditorMap(GamePane gamePane) {
        super(new EditorLevel(), gamePane);
        locker = new ReentrantLock();
    }
    
    /* 
        Getters
    */

    /* 
        Setters
    */

    
    /*
        Methodes
    */

    //Deplacement de la souris avec bouton enfoncé
    public void mouseDragged(int x, int y) {
        int gX = x/25;
        int gY = y/25;
        EditorCase _case = null;

        if(gX >= 0 && gX < grid.length && gY >= 0 &&  gY < grid[0].length)
        {
            _case = (EditorCase)grid[gX][gY];
            switch(((EditorPane)gamePane).getAction()) {
                case 0: //ajout de sol
                {
                    switch(((EditorPane)gamePane).getItem())
                    {
                        case 0: //Ajout d'herbe sur la case
                            _case.setCanHaveFire(true);
                            _case.setCanHaveTree(true);
                            _case.setCaseType(0);
                            removeTree(gX, gY);
                            removeFire(gX, gY);
                            _case.setTexture(Texture.Grass);
                        break;
                        case 1: //Ajout d'eau sur la case
                            _case.setCanHaveFire(false);
                            _case.setCanHaveTree(false);
                            _case.setCaseType(1);
                            removeTree(gX, gY);
                            removeFire(gX, gY);
                            _case.setTexture(Texture.Water);
                        break;
                        case 2: //Ajout d'une case morte
                            _case.setCanHaveFire(false);
                            _case.setCanHaveTree(true);
                            _case.setCaseType(2);
                            removeTree(gX, gY);
                            removeFire(gX, gY);
                            _case.setTexture(Texture.DeadFloor);
                        break;
                        case 3: //Ajout d'une case en pierre
                        _case.setCanHaveFire(false);
                        _case.setCanHaveTree(false);
                        _case.setCaseType(3);
                        removeTree(gX, gY);
                        removeFire(gX, gY);
                        _case.setTexture(Texture.StoneFloor);
                    break;
                    }
                }
                break;
                case 1: //ajout d'objet
                {
                    switch(((EditorPane)gamePane).getItem())
                    {
                        case 0:
                        {
                            //On vérifie que la case peut contenir un arbre
                            if(_case.canHaveTree())
                            {
                                //On bloque pour eviter les erreurs
                                locker.lock();
                                WeakTree t = new WeakTree(gX * Game.caseSD, gY * Game.caseSD, _case, this);
                                removeTree(gX, gY);
                                _case.addItem(t);
                                addItem(t);
                                //On debloque
                                locker.unlock();
                            }
                        }
                        break;
                        case 1:
                        {
                            //On vérifie que la case peut contenir un arbre
                            if(_case.canHaveTree())
                            {
                                //On bloque pour eviter les erreurs
                                locker.lock();
                                MediumTree t = new MediumTree(gX * Game.caseSD, gY * Game.caseSD, _case, this);
                                removeTree(gX, gY);
                                _case.addItem(t);
                                addItem(t);
                                //On debloque
                                locker.unlock();
                            }
                        }
                        break;
                        case 2:
                        {
                            //On vérifie que la case peut contenir un arbre
                            if(_case.canHaveTree())
                            {
                                //On bloque pour eviter les erreurs
                                locker.lock();
                                StrongTree t = new StrongTree(gX * Game.caseSD, gY * Game.caseSD, _case, this);
                                removeTree(gX, gY);
                                _case.addItem(t);
                                addItem(t);
                                //On debloque
                                locker.unlock();
                            }
                        }
                        break;
                        case 3:
                        {
                            //On vérifie que la case peut contenir un feu
                            if(_case.canHaveFire())
                            {
                                //On bloque pour eviter les erreurs
                                locker.lock();
                                Fire f = new Fire(_case, this);
                                removeFire(gX, gY);
                                _case.addItem(f);
                                addItem(f);
                                //On debloque
                                locker.unlock();
                            }
                        }
                        break;
                    }
                }
                break;
            }
        }
    }

    /*
        Supprime l'arbre sur la case x,y
    */
    public void removeTree(int x, int y) {
        Tree tree = null;
        ArrayList<LivingItem> items = grid[x][y].getItems();

        //On recherche l'arbre sur la case
        for (LivingItem i : items) {
            if(i instanceof Tree)
                tree = (Tree)i;
        }

        if(tree != null) {
            gamePane.remove(tree);
            removeItem(tree);
            grid[x][y].getItems().remove(tree);
        }
    }

    /*
        Supprime le feu sur la case x,y
    */
    public void removeFire(int x, int y) {
        Fire fire = null;
        ArrayList<LivingItem> items = grid[x][y].getItems();

        //On recherche l'arbre sur la case
        for (LivingItem i : items) {
            if(i instanceof Fire)
            fire = (Fire)i;
        }

        if(fire != null) {
            gamePane.remove(fire);
            removeItem(fire);
            grid[x][y].getItems().remove(fire);
        }
    }

    //Initialise la map sur la fenêtre de jeu
    public void init() {
        for (int x = 0; x < grid.length; x++) {
            for (int y = 0; y < grid[0].length; y++) {
                // Initialise la carte sur la fenêtre de jeu
                gamePane.add(grid[x][y]);
                //On place le sol à une profondeur de -5000 afin d'être sûr qu'il ne passe pas au dessus d'autres items
                gamePane.setLayer(grid[x][y], -5000);
                grid[x][y].getItems().forEach(i -> {
                    addItem(i);
                });
            }
        }


        started = true;
    }

    //Boucle principal de la map
    public void onLoop(Date now) {
        
    }

    //Sauvegarde de la map dans un fichier
    public void saveMap(File file) {
        try {
            file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.write(String.format("%d %d %d %d", grid.length, grid[0].length, basicFireFighterCount, strongFireFighterCount));
            for(int y = 0; y < grid[0].length; y++) {
                writer.write("\n");
                for(int x = 0; x < grid.length; x++) {
                    writer.write(String.format("%d ", ((EditorCase)grid[x][y]).caseType));
                }
            }

            for (LivingItem item : items) {
                writer.write(String.format("\n%d.%d.%d", item.getType(), item.getCase().getGridX(), item.getCase().getGridY()));
            }

            writer.close();
        } catch(IOException e) {
            
        }
    }

    //Chargement de la map à partir d'un fichier
    public void loadMap(File file) {
        BufferedReader reader;
        String[] data;
        String line;
        int y = 0;
        int x = 0;
        int itemX = 0;
        int itemY = 0;
        EditorCase _case = null;

        try {
            reader = new BufferedReader(new FileReader(file));
            line = reader.readLine();

            for(y = 0; y < grid[0].length; y++) {
                for(x = 0; x < grid.length; x++) {
                    gamePane.remove(grid[x][y]);
                }
            }
            y = 0;

            for(LivingItem i : items) {
                gamePane.remove(i);
            }

            items.clear();

            while(line != null) {
                data = line.split(" ");

                if(y == 0) { //Dimensions de la map et pompiers
                    grid = new Case[Integer.parseInt(data[0])][Integer.parseInt(data[1])];
                    
                    //Récupération des nombres de pompiers dispnibles
                    basicFireFighterCount = Integer.parseInt(data[2]);
                    strongFireFighterCount = Integer.parseInt(data[3]);

                } else if (y <= grid[0].length) { //Cases de la map
                    x = 0;
                    for(String t : data)  {
                        switch(t) {
                            case "0":
                                _case = new EditorCase(x, y - 1, x * Game.caseSD, (y - 1) * Game.caseSD, this, true, true);
                                _case.setTexture(Texture.Grass);
                                _case.setCaseType(0);
                            break;
                            case "1":
                                _case = new EditorCase(x, y - 1, x * Game.caseSD, (y - 1) * Game.caseSD, this, false, false);
                                _case.setTexture(Texture.Water);
                                _case.setCaseType(1);
                            break;
                            case "2":
                                _case = new EditorCase(x, y - 1, x * Game.caseSD, (y - 1) * Game.caseSD, this, false, false);
                                _case.setTexture(Texture.DeadFloor);
                                _case.setCaseType(2);
                            break;
                            case "3":
                                _case = new EditorCase(x, y - 1, x * Game.caseSD, (y - 1) * Game.caseSD, this, false, true);
                                _case.setTexture(Texture.StoneFloor);
                                _case.setCaseType(3);
                            break;
                        }
                        grid[x][y - 1] = _case;
                        x++;
                    }
                } else { //Objets
                    data = line.split("\\.");
                    itemX = Integer.parseInt(data[1]);
                    itemY = Integer.parseInt(data[2]);
                    _case = (EditorCase)grid[itemX][itemY];
                    switch(data[0]) {
                        case "0":
                            _case.setTree(new WeakTree(itemX * Game.caseSD, itemY * Game.caseSD, _case, this));
                        break;
                        case "1":
                            _case.setTree(new MediumTree(itemX * Game.caseSD, itemY * Game.caseSD, _case, this));
                        break;
                        case "2":
                            _case.setTree(new StrongTree(itemX * Game.caseSD, itemY * Game.caseSD, _case, this));
                        break;
                        case "20": //feu
                            _case.addItem(new Fire(_case, this));
                        break;
                    }
                }

                y++;
                line = reader.readLine();
            }
            
            reader.close();
            init();
            Game.getFrame().repaint();
        } catch(IOException e) {
            
        }
    }
}