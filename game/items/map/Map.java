package game.items.map;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.locks.ReentrantLock;
import game.Game;
import game.Texture;
import game.items.livingitems.LivingItem;
import game.items.livingitems.firefighters.*;
import game.items.map.levels.Level;
import game.windows.GamePane;
import game.windows.popup.EndPopup;
import game.items.map.cases.*;

public class Map {

    protected Level level; //Niveau à chargé pour la carte
    protected Case[][] grid; //Grille du jeu
    protected GamePane gamePane; //Pane pour l'affichage de la carte

    protected ArrayList<LivingItem> items; //Objets présent sur la carte
    protected ArrayList<LivingItem> toAddItems; //Objets à ajouter dans la liste en cours de boucle

    //Compte des pompiers disponibles
    protected int basicFireFighterCount, strongFireFighterCount;

    //Gestion des pompiers
    protected FireFighter selectedFireFighter;

    //Locker
    protected ReentrantLock locker;

    protected boolean started; //La carte est prête pour la boucle ou non

    public Map() {
        
    }

    public Map(Level level, GamePane gamePane) {
        this.level = level;
        this.gamePane = gamePane;

        this.items = new ArrayList<LivingItem>();
        this.toAddItems = new ArrayList<LivingItem>();

        this.locker = new ReentrantLock();

        // Récupération de la grille de jeu
        this.grid = level.getGrid(this);
    }
    
    /* 
        Getters
    */

    /*
        Retourne le nombre de colonnes de la grille
    */
    public int getXCase() {
        return level.getXCase();
    }

    /*
        Retourne le nombre de ligne de la grille
    */
    public int getYCase() {
        return level.getYCase();
    }

    /*
        Retourne la liste des objets
    */
    public ArrayList<LivingItem> getItems() {
        return items;
    }

    /*
        Retourne la case de la grille aux coordonnées X;Y
    */
    public Case getCase(int x, int y) {
        if(x < 0 || x >= getXCase() || y < 0 || y >= getYCase())
            return null;

        return grid[x][y];
    }

    /*
        Retourne la case de la grille aux coordonnées X;Y
    */
    public Case[][] getGrid() {
        return grid;
    }

    /*
       Retourne le pompier selectionné
    */
    public FireFighter getSelectedFireFighter() {
        return selectedFireFighter;
    }

    /*
        Retourne true si la map est prête
    */
    public boolean getStarted() {
        return started;
    }

    /*
        Retourne le nombre de pompiers normaux disponibles
    */
    public int getBasicFireFighterCount() {
        return basicFireFighterCount;
    }

    /*
        Retourne le nombre de pompiers fort disponibles
    */
    public int getStrongFireFighterCount() {
        return strongFireFighterCount;
    }

    /* 
        Setters
    */

    //Ajoute un objet à la liste des items en cours de boucle
    public void addItemLoop(LivingItem item) {
        //On doit bloquer la liste, pour éviter des soucis avec la boucle
        locker.lock();
        toAddItems.add(item);
        locker.unlock();
    }

    //Ajoute un objet à la liste des items
    public void addItem(LivingItem item) {
        items.add(0, item);
        gamePane.add(item, -1);
        gamePane.setLayer(item, item.getLayerPos());
    }

    //Supprime un objet à la liste des items
    public void removeItem(LivingItem item) {
        items.remove(item);
    }

    /*
        Selectionne un pompier
    */
    public void selectFireFighter(FireFighter f) {
        selectedFireFighter = f;
    }

    /*
        Modifie le nombre de pompiers normaux disponibles
    */
    public void setBasicFireFighterCount(int n) {
        basicFireFighterCount = n;
        if(basicFireFighterCount < 0)
            basicFireFighterCount = 0;
    }

    /*
        Modifie le nombre de pompiers fort disponibles
    */
    public void setStrongFireFighterCount(int n) {
        strongFireFighterCount = n;
        if(strongFireFighterCount < 0)
            strongFireFighterCount = 0;
    }

    /*
        Methodes
    */

    /*
        Action d'ajout de pompier
    */
    public void addFireFighter(Case _case) {
        FireFighter fighter = null;

        //Si la case n'est pas en feu, le pompier est créé
        if(!_case.haveFire()) {
            switch(Game.getPlaceFireFighter()) {
                case 0: //Ajout d'un pompier basique, si il en reste de disponibles
                    if(getBasicFireFighterCount() > 0) {
                        fighter = new BasicFireFighter(_case.getGridX(), _case.getGridY(), _case, this);
                        setBasicFireFighterCount(getBasicFireFighterCount() - 1);
                    }
                break;
                case 1: //Ajout d'un pompier fort, si il en reste de disponibles
                    if(getStrongFireFighterCount() > 0) {
                        fighter = new StrongFireFighter(_case.getGridX(), _case.getGridY(), _case, this);
                        setStrongFireFighterCount(getStrongFireFighterCount() - 1);
                    }
                break;
            }
        }

        //ajout du pompier
        if(fighter != null)
            addItemLoop(fighter);
        else Game.setPlaceFireFighter(-1);
    }

    //Selectionne une case vers la quelle se déplacé
    public void selectLocation(Case _case) {
        if(selectedFireFighter != null)
            selectedFireFighter.setTargetCase(_case);
    }

    //Change la position d'un item (sur le layeredpane)
    public void changeLayer(LivingItem item) {
        gamePane.setLayer(item, item.getLayerPos());
    }

    // Initialise la map sur la fenêtre de jeu
    public void init() {
        //Initialisation des nombres de pompiers
        setBasicFireFighterCount(level.getBasicFireFighterCount());
        setStrongFireFighterCount(level.getStrongFireFighterCount());
        for (int x = 0; x < grid.length; x++) {
            for (int y = 0; y < grid[0].length; y++) {
                // Initialise la carte sur la fenêtre de jeu
                gamePane.add(grid[x][y]);
                //On place le sol à une profondeur de -5000 afin d'être sûr qu'il ne passe pas au dessus d'autres items
                gamePane.setLayer(grid[x][y], -5000);
                grid[x][y].getItems().forEach(i -> {
                    addItem(i);
                });
            }
        }
        started = true;
    }

    //Action avec souris
    public void mouseDragged(int x, int y) {
        
    }

    //Changement de la case
    public void changeCase(int x, int y, Case c) {
        Case c2 = getCase(x, y);
        for(LivingItem i : c2.getItems()) {
            c.getItems().add(i);
        }
        grid[x][y] = c;
        gamePane.remove(c2);
        gamePane.add(c);
        gamePane.setLayer(c, -5000);
    }

    //Boucle principale de la map
    public void onLoop(Date now) {
        ArrayList<LivingItem> removeItems = new ArrayList<LivingItem>();
        int activeFire = 0;
        Case _case = null;

        //On utilise la fonction de boucle de chaque case
        //Pour répandre le feu si besoin
        for(int x = 0; x < grid.length; x++) {
            for(int y = 0; y < grid[0].length; y++) {
                _case = getCase(x, y);
                _case.onLoop(now);
                if(_case.haveFire() && _case.haveActivFire())
                    activeFire++;
            }
        }
        
        items.forEach(i -> {
            i.onLoop(now);
            if (i.canRemove())
                removeItems.add(i);
        });

        //Ajout des items 
        //On doit bloquer la liste, pour éviter des soucis avec la boucle
        locker.lock();
        toAddItems.forEach(i -> {
            i.getCase().addItem(i);
            addItem(i);
        });
        toAddItems.clear();
        locker.unlock();

        //Suppression des éléments à supprimé
        removeItems.forEach(i -> {
            removeItem(i);
            i.getCase().removeItem(i);
            gamePane.remove(i);
        });
        removeItems.clear();

        if(activeFire <= 0) {
            /*
                Si tous les feus sont maitrisés, la partie est gagnée
            */
            new EndPopup(Game.getTreeCount(), Game.getPoints(), Game.getFireFighterDeadCount(), ((gamePane.getWidth() / 2) - 190), ((gamePane.getHeight() / 2) - 85), 380, 170, gamePane, Texture.WinImage.getImage());
            gamePane.repaint();
            started = false;
        } else if(Game.getTreeCount() <= 0 || ((level.getBasicFireFighterCount() + level.getStrongFireFighterCount() - Game.getFireFighterDeadCount()) <= 0)) {
            /* 
                S'il n'y a plus d'arbre en vie ou plus de pompier disponible
                la partie est perdue
            */
            new EndPopup(0, Game.getPoints(), Game.getFireFighterDeadCount(), ((gamePane.getWidth() / 2) - 190), ((gamePane.getHeight() / 2) - 85), 380, 170, gamePane, Texture.LoseImage.getImage());
            gamePane.repaint();
            started = false;
        }
    }

    //Chargement d'une carte à partir d'un fichier
    public void loadMap(File file) {
        
    }

    //Destruction d'une carte
    public void finalize() {
        items.clear();
        grid = null;
        started = false;
    }
}