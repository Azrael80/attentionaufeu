package game.items.map.cases;

import java.awt.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

import game.Game;
import game.Texture;
import game.items.livingitems.*;
import game.items.livingitems.trees.*;
import game.items.map.Map;
import game.items.livingitems.firefighters.*;
import game.windows.Animation;
import game.windows.GameComponent;

abstract public class Case extends GameComponent implements Animation {
    
    private static final long serialVersionUID = 1L;

    // Coordonnées de la case sur la grille
    protected int gridX, gridY;

    //Carte sur la quelle se trouve l'objet
    protected Map map;

    //Liste des objets sur la case
    protected ArrayList<LivingItem> items;

    //Animation
    protected Hashtable<Integer, Image> frames;
    protected int frameCount;

    //Information sur le contenu (si elle contient un arbre ou un feu)
    protected boolean haveTree, haveFire;

    //Prochain feu
    protected Date nextFire;
    protected boolean noPlaceForFire;

    public Case(int gridX, int gridY, int x, int y, Map map) {
        super(x, y);

        this.gridX = gridX;
        this.gridY = gridY;
        this.map = map;
        this.items = new ArrayList<LivingItem>();

        haveTree = false;
        haveFire = false;
        nextFire = new Date(new Date().getTime() + 3500);

        /*
            Animation de la case 
            0: Case normale
            1: Case selectionnée
        */
        frames = new Hashtable<Integer, Image>();

        frameCount = 0;
    }

    //Getters

    //Retourne la colone de la case sur la grille
    public int getGridX() {
        return gridX;
    }

    //Retourne la ligne de la case sur la grille
    public int getGridY() {
        return gridY;
    }

    /*
        Retourne la liste des objets (arbre, feu, joueur) sur la case
    */
    public ArrayList<LivingItem> getItems() {
        return items;
    }

    /*
        Retourne l'arbre sur la case
    */
    public Tree getTree() {
        Tree t = null;
        for(LivingItem i : getItems())
            if(i instanceof Tree)
                t = (Tree)i;
        return t;
    }

    /*
        Retourne true, si la case contient un arbre
    */
    public boolean haveTree() {
        return haveTree;
    }

    /*
        Retourne true, si la case contient un feu
    */
    public boolean haveFire() {
        return haveFire;
    }

    /*
        Retourne true, si la case a un feu vraiment actif
        (feu qui peut toujours se propager ou qui touche un arbre encore vivant)
    */
    public boolean haveActivFire() {
        return haveFire && (noPlaceForFire == false || haveTree && getTree().getDead() == false);
    }

    //Setters

    public void setTexture(Texture t) {

    }

    /*
        Ajout un objet sur la case
    */
    public void addItem(LivingItem item) {
        if(item instanceof Fire) //s'il s'agit d'un feu, alors on modifie le paramètre
            haveFire = true;
        if(item instanceof Tree) //s'il s'agit d'un arbre, alors on modifie le paramètre
            haveTree = true;
            
        items.add(0, item);
    }

    /*
        Supprime un objet sur la case
    */
    public void removeItem(LivingItem item) {
        //si l'objet est un feu, la case n'est plus en feu
        //et est considérée comme brûlée
        if(item instanceof Fire) 
        {
            haveFire = false;
            DeadCase c = new DeadCase(gridX, gridY, getX(), getY(), map);
            map.changeCase(gridX, gridY, c);
        }
        items.remove(item);
    }

    /*
        Ajoute un arbre sur la case
    */
    public void setTree(Tree tree) {
        addItem(tree);
        haveTree = true;
    }

    /*
        Ajoute un feu sur la case
    */
    public void setFire(Fire fire) {
        map.addItemLoop(fire);
        haveFire = true;
        updateNextFire(new Date());
    }

    public void onLoop(Date now) {
        //Si la case contient un feu
        if(haveFire)
        {
            //On répand le feu
            if(now.compareTo(nextFire) >= 0) {
                spreadFire(now);
            }
        }
    }

    //Répand le feu sur une case aléatoire dans un rayon de 1 case
    public void spreadFire(Date now) {
        ArrayList<Case> cases = new ArrayList<Case>();
        Case _case = null;

        //Selection de toutes les cases autour de celle-ci 
        for(int x = gridX - 1; x <= gridX + 1; x++) {
            for(int y = gridY - 1; y <= gridY + 1; y++) {
                if(x != gridX || y != gridY) {
                    _case = map.getCase(x, y);
                    if(_case != null && _case.canHaveFire() && _case.haveFire == false)
                        cases.add(_case);
                }
            }
        }
        _case = null;

        /*
            Si aucune case n'a été trouvé, c'est qu'il n'y a plus de place pour de noveaux feu
            autour de la case, on enregistre donc le resultat (l'utilisateur aura donc 'contrôlé' ce feu sans l'éteindre)
        */
        if(cases.size() > 0)
            _case = cases.get(Game.getRandomNumberInRange(0, cases.size() - 1));
        else 
            noPlaceForFire = true;

        if(_case != null)
        {
            _case.setFire(new Fire(_case, map));
        }

        updateNextFire(now);
    }

    //Ajouter des millisecondes au compteur pour la prochaine fois que le feu devra se répandre
    public void updateNextFire(Date now) {
        nextFire = new Date(now.getTime() + Game.getRandomNumberInRange(3500, 10000));
    }

    public void paint(Graphics g) {
        g.drawImage(getFrame(), 0, 0, null);
        //Si la case est selectionnée et qu'un pompier doit être placé, on affiche son fantôme (seulement si il n'y a pas de feu)
        if(frameCount == 1 && !haveFire && Game.getPlaceFireFighter() > - 1)
            g.drawImage(Game.getPlaceFireFighter() == 0 ? Texture.BasicFireFighterFantom.getImage() : Texture.StrongFireFighterFantom.getImage(), 0, 0, null);
    }

    /*
        Methodes
    */

    /*
        Retourne le premier pompier dans la case
    */
    public FireFighter getFirstFireFighter() {
        FireFighter f = null;
        for(int i = 0; i < items.size() && f == null; i++) {
            if(items.get(i) instanceof FireFighter)
            {
                f = (FireFighter)items.get(i);
            }
        }
        return f;
    }

    /*
        Interface animation
    */

    public void setFrame(int n) {
        frameCount = n; 
    }

    public Image getFrame() {
        return frames.get(frameCount);
    }

    //Methodes abstraites
    abstract public boolean canWalk(FireFighter f);
    abstract public boolean canHaveTree();
    abstract public boolean canHaveFire();
}
