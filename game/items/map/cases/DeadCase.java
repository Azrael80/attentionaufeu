package game.items.map.cases;

import game.Game;
import game.Texture;
import game.items.livingitems.firefighters.FireFighter;
import game.items.map.Map;
import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.SwingUtilities;

public class DeadCase extends Case {

    private static final long serialVersionUID = 1L;

    public DeadCase(int gridX, int gridY, int x, int y, Map map) {
        super(gridX, gridY, x, y, map);

        /*
            Animation de la case 
            0: Case normale
            1: Case normale
        */
        frames.put(0, Texture.DeadFloor.getImage());
        frames.put(1, Texture.DeadFloor.getImage());

        frameCount = 0;

        addMouseListener(new MouseAdapter() {
             //Changement de texture quand la souris entre/sort
             @Override
             public void mouseEntered(MouseEvent e) {
                 setFrame(1);
                 Game.changeCursor(new Cursor(Cursor.HAND_CURSOR));
             }
 
             @Override
             public void mouseExited(MouseEvent e) {
                 setFrame(0);
                 Game.changeCursor(new Cursor(Cursor.DEFAULT_CURSOR));
             }
 
             @Override
             public void mousePressed(MouseEvent e) {
                 //Si le placement de pompier n'est pas en cours, action basique
                 if(Game.getPlaceFireFighter() == - 1) {
                     FireFighter f = getFirstFireFighter();
                     if(f != null && f.getDead() == false && f != map.getSelectedFireFighter())
                         map.selectFireFighter(f);
                     else {
                         map.selectLocation((Case)e.getComponent());
                     }
                 } else  {
                     //Si le clique droit est activé, on retire le placement de pompier
                     //sinon on place un pompier
                     if(SwingUtilities.isRightMouseButton(e)) {
                         Game.setPlaceFireFighter(-1);
                     } else {          
                         map.addFireFighter((Case)e.getComponent());
                     }
                 }
             }
        });
    }

    //Case morte, donc pas de feu
    public boolean canWalk(FireFighter f) {
        return true;
    }

    public boolean canWalk() {
        return true;
    }

    public boolean canHaveTree() {
        return true;
    }

    public boolean canHaveFire() {
        return false;
    }
}