package game.items.map.cases;

import game.Texture;
import game.items.livingitems.firefighters.FireFighter;
import game.items.map.Map;
import java.awt.*;

public class EditorCase extends Case {

    private static final long serialVersionUID = 1L;

    Texture texture = null;
    public int caseType = 0;

    //La case peut contenir un feu/arbre ou non
    boolean canHaveFire, canHaveTree;

    public EditorCase(int gridX, int gridY, int x, int y, Map map, boolean canHaveFire, boolean canHaveTree) {
        super(gridX, gridY, x, y, map);

        this.canHaveFire = canHaveFire;
        this.canHaveTree = canHaveTree;

        /*
            Animation de la case 
            0: Case normale
        */
        frames.put(0, Texture.Water.getImage());

        frameCount = 0;

        
    }

    //change la valeur de canHaveFire
    public void setCanHaveFire(boolean b) {
        canHaveFire = b;
    }

    //change la valeur de canHaveTree
    public void setCanHaveTree(boolean b) {
        canHaveTree = b;
    }

    //Change le type de case
    public void setCaseType(int t) {
        caseType = t;
    }

    @Override
    public void setTexture(Texture t) {
        texture = t;
        repaint();
    }

    @Override
    public void paint(Graphics g) {
        if(texture != null) 
            g.drawImage(texture.getImage(), 0, 0, null);
        g.drawRect(0, 0, 25, 25);
    }

    @Override
    public boolean canWalk(FireFighter f) {
        return false;
    }

    @Override
    public boolean canHaveTree() {
        return canHaveTree;
    }

    @Override
    public boolean canHaveFire() {
        return canHaveFire;
    }
}