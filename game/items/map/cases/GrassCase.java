package game.items.map.cases;

import game.Texture;
import game.items.map.Map;

public class GrassCase extends WalkableCase {

    private static final long serialVersionUID = 1L;

    public GrassCase(int gridX, int gridY, int x, int y, Map map) {
        super(gridX, gridY, x, y, map);

        /*
            Animation de la case 
            0: Case normale
            1: Case selectionnée
        */
        frames.put(0, Texture.Grass.getImage());
        frames.put(1, Texture.Grass2.getImage());
    }

    public boolean canHaveTree() {
        return true;
    }

    public boolean canHaveFire() {
        return true;
    }
}