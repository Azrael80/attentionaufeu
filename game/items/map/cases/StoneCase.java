package game.items.map.cases;

import game.Texture;
import game.items.map.Map;

public class StoneCase extends WalkableCase {

    private static final long serialVersionUID = 1L;

    public StoneCase(int gridX, int gridY, int x, int y, Map map) {
        super(gridX, gridY, x, y, map);

        /*
            Animation de la case 
            0: Case normale
            1: Case normale
        */
        frames.put(0, Texture.StoneFloor.getImage());
        frames.put(1, Texture.StoneFloorOver.getImage());

    }

    public boolean canHaveTree() {
        return false;
    }

    public boolean canHaveFire() {
        return false;
    }
}