package game.items.map.cases;

import game.Game;
import game.items.livingitems.firefighters.FireFighter;
import game.items.map.Map;
import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.SwingUtilities;

abstract public class WalkableCase extends Case {

    private static final long serialVersionUID = 1L;

    public WalkableCase(int gridX, int gridY, int x, int y, Map map) {
        super(gridX, gridY, x, y, map);
        frameCount = 0;

        addMouseListener(new MouseAdapter() {
            //Changement de texture quand la souris entre/sort
            @Override
            public void mouseEntered(MouseEvent e) {
                setFrame(1);
                Game.changeCursor(new Cursor(Cursor.HAND_CURSOR));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                setFrame(0);
                Game.changeCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }

            @Override
            public void mousePressed(MouseEvent e) {
                //Si le placement de pompier n'est pas en cours, action basique
                if(Game.getPlaceFireFighter() == - 1) {
                    FireFighter f = getFirstFireFighter();
                    if(f != null && f.getDead() == false && f != map.getSelectedFireFighter())
                        map.selectFireFighter(f);
                    else {
                        map.selectLocation((Case)e.getComponent());
                    }
                } else  {
                    //Si le clique droit est activé, on retire le placement de pompier
                    //sinon on place un pompier
                    if(SwingUtilities.isRightMouseButton(e)) {
                        Game.setPlaceFireFighter(-1);
                    } else {          
                        map.addFireFighter((Case)e.getComponent());
                    }
                }
            }
        });
    }

    public boolean canWalk(FireFighter f) {
        /*
            Pour pouvoir marcher sur la case, il faut qu'elle ne soit pas en feu
            le joueur peut passer à travers les arbres, et il peut toujours marcher
            sur sa propre case
        */
        return !haveFire || f.getCase() == this;
    }

    abstract public boolean canHaveTree();

    abstract public boolean canHaveFire();
}