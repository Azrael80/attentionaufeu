package game.items.map.cases;

import game.Texture;
import game.items.livingitems.firefighters.FireFighter;
import game.items.map.Map;

public class WaterCase extends Case {

    private static final long serialVersionUID = 1L;

    public WaterCase(int gridX, int gridY, int x, int y, Map map) {
        super(gridX, gridY, x, y, map);

        /*
            Animation de la case 
            0: Case normale
        */
        frames.put(0, Texture.Water.getImage());

        frameCount = 0;
    }

    //Pas possible de marcher sur l'eau
    public boolean canWalk(FireFighter f) {
        return false;
    }

    public boolean canHaveTree() {
        return false;
    }

    public boolean canHaveFire() {
        return false;
    }
}