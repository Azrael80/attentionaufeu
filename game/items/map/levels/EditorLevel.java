package game.items.map.levels;

import game.Game;
import game.Texture;
import game.items.map.Map;
import game.items.map.cases.Case;
import game.items.map.cases.EditorCase;

public class EditorLevel extends Level {

    public EditorLevel() {
        xCase = 35;
        yCase = 25;
    }

    @Override
    public Case[][] getGrid(Map map) {
        Case[][] grid = new Case[xCase][yCase];
        int x = 0;
        int y = 0;

        for(x = 0; x < xCase; x++) {
            for(y = 0; y < yCase; y++)
            {
                grid[x][y] = new EditorCase(x, y, x * Game.caseSD, y * Game.caseSD, map, true, true);
                grid[x][y].setTexture(Texture.Grass);
            }
        }

        return grid;
    }
}