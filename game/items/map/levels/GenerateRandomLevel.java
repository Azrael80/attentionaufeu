package game.items.map.levels;

import java.util.Random;

import game.Game;
import game.items.livingitems.Fire;
import game.items.livingitems.trees.*;
import game.items.map.Map;
import game.items.map.cases.Case;
import game.items.map.cases.GrassCase;
import game.items.map.cases.WaterCase;

public class GenerateRandomLevel extends Level {

    public GenerateRandomLevel() {
        xCase = 35;
        yCase = 25;
        basicFireFighterCount = 25;
        strongFireFighterCount = 12;
    }

    private int getRandomNumberInRange(int min, int max) {
        if (min > max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        if (min == max)
            return min;

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    /*
     * Génère une grille de jeu aléatoire
     */
    public Case[][] getGrid(Map map) {
        int x, y;
        Case _case = null;
        Tree tree = null;
        Case[][] grid = new Case[xCase][yCase];

        for(x = 0; x < xCase; x++) {
            for(y = 0; y < yCase; y++)
            {
                if(getRandomNumberInRange(0, 100000) < 3000) 
                    grid[x][y] = new WaterCase(x, y, x * Game.caseSD, y * Game.caseSD, map);
                else
                    grid[x][y] = new GrassCase(x, y, x * Game.caseSD, y * Game.caseSD, map);
            }
        }

        for (int i = 0; i < 400; i++) {
            x = getRandomNumberInRange(0, 34);
            y = getRandomNumberInRange(0, 24);
            int t = getRandomNumberInRange(1, 3);
            _case = grid[x][y];
            boolean add = true;

            // Si la case contient déjà un arbre, pas d'ajout d'autre arbre
            if (_case.haveTree() || !_case.canHaveTree())
                add = false;

            // Ajout de l'arbre à la case
            if (add) {
                if (t == 1) {
                    tree = new StrongTree(x * Game.caseSD, y * Game.caseSD, _case, map);
                } else if (t == 2) {
                    tree = new MediumTree(x * Game.caseSD, y * Game.caseSD, _case, map);
                } else {
                    tree = new WeakTree(x * Game.caseSD, y * Game.caseSD, _case, map);
                }
                _case.setTree(tree);

                //1 chance sur 8 que l'arbre soit en feu
                if(getRandomNumberInRange(0, 100000) <= 1250) {
                    _case.setFire(new Fire(_case, map));
                }
            }
        }
        return grid;
    }
}