package game.items.map.levels;

import game.items.map.*;
import game.items.map.cases.*;

abstract public class Level {
    protected int xCase, yCase;

    //Compte des pompiers disponibles
    protected int basicFireFighterCount, strongFireFighterCount;

    /*
        Retourne le nombre de colonnes de la grille
    */
    public int getXCase() {
        return xCase;
    }

    /*
        Retourne le nombre de ligne de la grille
    */
    public int getYCase() {
        return yCase;
    }

    /*
        Retourne le nombre de pompiers normaux disponibles
    */
    public int getBasicFireFighterCount() {
        return basicFireFighterCount;
    }

    /*
        Retourne le nombre de pompiers fort disponibles
    */
    public int getStrongFireFighterCount() {
        return strongFireFighterCount;
    }

    /*
        Créer une grille de jeu à partir des données du niveau
    */
    abstract public Case[][] getGrid(Map map);
}