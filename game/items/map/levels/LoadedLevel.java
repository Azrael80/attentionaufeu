/*
    Classe pour les niveaux chargés à partir de fichier .lvl
*/

package game.items.map.levels;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import game.Game;
import game.items.livingitems.Fire;
import game.items.livingitems.trees.*;
import game.items.map.Map;
import game.items.map.cases.Case;
import game.items.map.cases.DeadCase;
import game.items.map.cases.GrassCase;
import game.items.map.cases.StoneCase;
import game.items.map.cases.WaterCase;

public class LoadedLevel extends Level {

    private File file;

    public LoadedLevel(File file) {
        super();
        this.file = file;
    }

    /*
     Génère la grille de jeu à partir du fichier
    */
    public Case[][] getGrid(Map map) {
        Case _case = null;
        Case[][] grid = new Case[xCase][yCase];
        BufferedReader reader;
        String[] data;
        String line;
        int y = 0;
        int x = 0;
        int itemX = 0;
        int itemY = 0;

        try {
            file.createNewFile();
            reader = new BufferedReader(new FileReader(file));
            line = reader.readLine();

            while(line != null) {
                data = line.split(" ");

                if(y == 0) { //Dimensions de la map
                    xCase = Integer.parseInt(data[0]);
                    yCase = Integer.parseInt(data[1]);
                    basicFireFighterCount = Integer.parseInt(data[2]);
                    strongFireFighterCount = Integer.parseInt(data[3]);
                    grid = new Case[xCase][yCase];
                } else if (y <= grid[0].length) { //Cases de la map
                    x = 0;
                    for(String t : data)  {
                        switch(t) {
                            case "0":
                                _case = new GrassCase(x, y - 1, x * Game.caseSD, (y - 1) * Game.caseSD, map);
                            break;
                            case "1":
                                _case = new WaterCase(x, y - 1, x * Game.caseSD, (y - 1) * Game.caseSD, map);
                            break;
                            case "2":
                                _case = new DeadCase(x, y - 1, x * Game.caseSD, (y - 1) * Game.caseSD, map);
                            break;
                            case "3":
                                _case = new StoneCase(x, y - 1, x * Game.caseSD, (y - 1) * Game.caseSD, map);
                            break;
                        }
                        grid[x][y - 1] = _case;
                        x++;
                    }
                } else { //Objets
                    data = line.split("\\.");
                    itemX = Integer.parseInt(data[1]);
                    itemY = Integer.parseInt(data[2]);
                    _case = grid[itemX][itemY];
                    switch(data[0]) {
                        case "0":
                            _case.addItem(new WeakTree(itemX * Game.caseSD, itemY * Game.caseSD, _case, map));
                        break;
                        case "1":
                            _case.addItem(new MediumTree(itemX * Game.caseSD, itemY * Game.caseSD, _case, map));
                        break;
                        case "2":
                            _case.addItem(new StrongTree(itemX * Game.caseSD, itemY * Game.caseSD, _case, map));
                        break;
                        case "20": //feu
                            _case.addItem(new Fire(_case, map));
                        break;
                    }
                }

                y++;
                line = reader.readLine();
            }
            
            reader.close();
        } catch(IOException e) {
            
        }
        return grid;
    }
}