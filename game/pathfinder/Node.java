package game.pathfinder;


import game.items.map.cases.Case;

public class Node {
    protected double cost_g, cost_h, cost_f;
    protected Case _case;
    protected Node parent;

    public Node(double cost_g, double cost_h, double cost_f, Case _case, Node parent) {
        this.cost_g = cost_g;
        this.cost_h = cost_h;
        this.cost_f = cost_f;
        this._case = _case;
        this.parent = parent;
    }


    public Case getCase() {
        return _case;
    }
}