package game.pathfinder;

import java.util.ArrayList;
import game.items.livingitems.firefighters.FireFighter;
import game.items.map.cases.Case;

/*
    Implementation A*
*/

public class PathFinder {

    private ArrayList<Node> openList, closeList;
    private Case target;
    private FireFighter fireFighter;

    public PathFinder(FireFighter f, Case[][] map, Case start, Case end) {
        fireFighter = f;
        Node actual = new Node(0, distance(start, end), distance(start, end), start, null);
        target = end;

        openList = new ArrayList<Node>();
        closeList = new ArrayList<Node>();

        openList.add(actual);
        
        while(actual.getCase() != end && openList.size() > 0) {
            actual = getBestNode(openList);
            addCloseList(actual);
            addAdjacentCase(map, actual);
        }

    }

    public void addAdjacentCase(Case[][] map, Node n) {
        Node tmp = null;
        Node exist = null;
        Case c = null;
        double cost_g, cost_h, cost_f;

        for(int i = n.getCase().getGridX() - 1; i <= n.getCase().getGridX() + 1; i++) {
            //En dehors du tableau
            if((i < 0) || (i >= map.length))
                continue;
            for(int j = n.getCase().getGridY() - 1; j <= n.getCase().getGridY() + 1; j++) {
                //En dehors du tableau
                if((j < 0) || (j >= map[0].length))
                    continue;

                //Case actuelle
                if(map[i][j] == n.getCase())
                    continue;

                //Case non franchissable
                if(map[i][j].haveFire() || !map[i][j].canWalk(this.fireFighter))
                    continue;

                c = map[i][j];

                if(!inNodeList(c, closeList)) {
                    cost_g = n.cost_g + distance(c, n.getCase());
                    cost_h = distance(c, target);
                    cost_f = cost_g + cost_h;
                    tmp = new Node(cost_g, cost_h, cost_f, c, n);

                    //System.out.format("%d %d %f\n", i, i, tmp.cost_f);
                    exist = getInNodeList(c, openList);

                    if(exist != null) {
                        if(tmp.cost_f < exist.cost_f) {
                            openList.remove(exist);
                            openList.add(tmp);
                        }
                    } else {
                        openList.add(tmp);
                    }
                }
            }
        }
    }

    //Retourne le chemin complet
    public ArrayList<Case> getPath() {
        ArrayList<Case> path = new ArrayList<Case>();
        Node end = getInNodeList(target, closeList);

        while(end != null) {
            path.add(0, end.getCase());
            end = end.parent;
        }
        
        return path;
    }

    //Supprime le noeud de la liste ouverte, et le met en liste fermée
    public void addCloseList(Node n) {
        closeList.add(n);
        openList.remove(n);
    }

    static public double distance(Case c1, Case c2) {
        return Math.sqrt((c1.getGridX() - c2.getGridX()) * (c1.getGridX() - c2.getGridX()) + (c1.getGridY() - c2.getGridY()) * (c1.getGridY() - c2.getGridY()));
    }

    //Retourne le meilleur noeud de la liste
    static public Node getBestNode(ArrayList<Node> nodes) {
        Node node = nodes.get(0);

        for(Node n : nodes) {
            if(n.cost_f < node.cost_f)
                node = n;
        }

        return node;
    }

    static public Node getInNodeList(Case _case, ArrayList<Node> nodes) {
        for(Node n : nodes)
            if(n.getCase() == _case)
                return n;
        return null;
    }

    static public boolean inNodeList(Case _case, ArrayList<Node> nodes) {
        return getInNodeList(_case, nodes) != null;
    }
    
}