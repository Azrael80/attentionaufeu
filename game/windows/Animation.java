package game.windows;

import java.awt.Image;

public interface Animation {

    public void setFrame(int n);
    public Image getFrame();

}