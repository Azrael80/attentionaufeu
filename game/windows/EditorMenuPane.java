package game.windows;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import game.Game;
import game.Texture;
import game.items.map.EditorMap;

public class EditorMenuPane extends JLayeredPane {

    private static final long serialVersionUID = 1L;
    protected EditorPane pane;
    protected JPanel basicFireFighterPanel;
    protected JPanel strongFireFighterPanel;

    public EditorMenuPane(EditorPane pane) {
        super();
        this.pane = pane;
        setBounds(Game.getPane().getWidth(), 0, 205, Game.getPane().getHeight());
        setLayout(new FlowLayout());


        //Ajout des boutons
        //Texture herbe 1
        add(newButton(Texture.Grass, 8, 68, 25, 25, new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.HAND_CURSOR));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }

            //Passage de l'action sur dessin de l'herbe
            @Override
            public void mousePressed(MouseEvent e) {
                pane.setAction(0, 0);
            }
        }));

        //Texture eau 1
        add(newButton(Texture.Water, 37, 68, 25, 25, new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.HAND_CURSOR));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }

            //Passage de l'action sur dessin de l'eau
            @Override
            public void mousePressed(MouseEvent e) {
                pane.setAction(0, 1);
            }
        }));

        //Texture sol mort
        add(newButton(Texture.DeadFloor, 66, 68, 25, 25, new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.HAND_CURSOR));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }

            //Passage de l'action sur dessin de l'eau
            @Override
            public void mousePressed(MouseEvent e) {
                pane.setAction(0, 2);
            }
        }));

        //Texture sol pierre
        add(newButton(Texture.StoneFloor, 95, 68, 25, 25, new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.HAND_CURSOR));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }

            //Passage de l'action sur dessin de l'eau
            @Override
            public void mousePressed(MouseEvent e) {
                pane.setAction(0, 3);
            }
        }));

        //Objet arbre 1
        add(newButton(Texture.WeakTree, 8, 162, 25, 25, new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.HAND_CURSOR));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }

            //Passage de l'action sur ajout d'objet
            @Override
            public void mousePressed(MouseEvent e) {
                pane.setAction(1, 0);
            }
        }));

        //Objet arbre 2
        add(newButton(Texture.MediumTree, 37, 162, 25, 25, new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.HAND_CURSOR));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }

            //Passage de l'action sur ajout d'objet
            @Override
            public void mousePressed(MouseEvent e) {
                pane.setAction(1, 1);
            }
        }));

        //Objet arbre 3
        add(newButton(Texture.StrongTree, 66, 162, 25, 25, new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.HAND_CURSOR));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }

            //Passage de l'action sur ajout d'objet
            @Override
            public void mousePressed(MouseEvent e) {
                pane.setAction(1, 2);
            }
        }));

        //Objet feu
        add(newButton(Texture.Fire1, 95, 162, 25, 25, new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.HAND_CURSOR));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }

            //Passage de l'action sur ajout d'objet
            @Override
            public void mousePressed(MouseEvent e) {
                pane.setAction(1, 3);
            }
        }));

        //Bouton chargement
        add(newButton(Texture.EditorCharge, 6, 334, 91, 38, new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.HAND_CURSOR));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }

            @Override
            public void mousePressed(MouseEvent e) {
                Game.openLoadMap(pane.map);
            }
        }));

        //Bouton sauvegarde
        add(newButton(Texture.EditorSave, 105, 334, 91, 38, new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.HAND_CURSOR));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }

            @Override
            public void mousePressed(MouseEvent e) {
                Game.openSaveEditor((EditorMap)pane.map);
            }
        }));

        //Bouton menu
        add(newButton(Texture.HomeEditorButton, 51, 377, 91, 38, new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.HAND_CURSOR));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }

            @Override
            public void mousePressed(MouseEvent e) {
                Game.openStartMenu();
            }
        }));

        //Input pompier
        basicFireFighterPanel = new JPanel() {

			private static final long serialVersionUID = 1L;

			@Override
            public void paint(Graphics g) {
                super.paint(g);
                g.setColor(Color.BLACK);
                g.setFont(new Font("default", Font.BOLD, 16));
                g.drawImage(Texture.FireFighter00.getImage(), 0, 5, null);
                g.drawString(String.format("x%d", pane.map.getBasicFireFighterCount()), 27, 30);
                setBounds(6, 234, 91, 38);
            }
        };

        //Bouton plus pompier faible
        basicFireFighterPanel.add(newButton(Texture.EditorButtonPlus, 79, 2, 10, 10,  new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.HAND_CURSOR));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }

            @Override
            public void mousePressed(MouseEvent e) {
                pane.map.setBasicFireFighterCount(pane.map.getBasicFireFighterCount() + 1);
                repaint();
            }
        }));
        
        //Bouton moins pompier faible
        basicFireFighterPanel.add(newButton(Texture.EditorButtonLess, 79, 25, 10, 10, new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.HAND_CURSOR));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }

            @Override
            public void mousePressed(MouseEvent e) {
                pane.map.setBasicFireFighterCount(pane.map.getBasicFireFighterCount() - 1);
                repaint();
            }
        }));

        strongFireFighterPanel = new JPanel() {

			private static final long serialVersionUID = 1L;

			@Override
            public void paint(Graphics g) {
                super.paint(g);
                g.setColor(Color.BLACK);
                g.setFont(new Font("default", Font.BOLD, 16));
                g.drawImage(Texture.StrongFireFighter00.getImage(), 0, 5, null);
                g.drawString(String.format("x%d", pane.map.getStrongFireFighterCount()), 27, 30);
                setBounds(105, 234, 91, 38);
            }
        };

        //Bouton plus pompier fort
        strongFireFighterPanel.add(newButton(Texture.EditorButtonPlus, 79, 2, 10, 10,  new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.HAND_CURSOR));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }

            @Override
            public void mousePressed(MouseEvent e) {
                pane.map.setStrongFireFighterCount(pane.map.getStrongFireFighterCount() + 1);
                repaint();
            }
        }));
        
        //Bouton moins pompier fort
        strongFireFighterPanel.add(newButton(Texture.EditorButtonLess, 79, 25, 10, 10, new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.HAND_CURSOR));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }

            @Override
            public void mousePressed(MouseEvent e) {
                pane.map.setStrongFireFighterCount(pane.map.getStrongFireFighterCount() - 1);
                repaint();
            }
        }));

        add(basicFireFighterPanel);
        add(strongFireFighterPanel);
    }


    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        //Affichage des titres
        g.drawImage(Texture.EditorTitle.getImage(), 32, 7, null);
        g.drawImage(Texture.EditorSols.getImage(), 6, 47, null);
        g.drawImage(Texture.EditorObjets.getImage(), 6, 141, null);
    }


    /*
        Création d'un bouton pour le menu
    */
    static public JPanel newButton(Texture texture, int x, int y, int width, int height, MouseAdapter action) {
        JPanel button = new JPanel() {
            private static final long serialVersionUID = 1L;

            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(texture.getImage(), 0, 0, null);
                setBounds(x, y, width, height);
            }
        };

        button.addMouseListener(action);

        return button;
    }

}