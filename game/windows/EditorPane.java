package game.windows;

import game.Game;
import game.items.map.EditorMap;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class EditorPane extends GamePane {

    private static final long serialVersionUID = 1L;
    
    int action;
    int item;

    /*
        Initialise la panel
    */
    public EditorPane() {
        super();
        this.map = new EditorMap(this);

        setBackground(Color.RED);

        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.HAND_CURSOR));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }
        });

        addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                //Si la souris est pressée, on applique l'action de l'editeur
                map.mouseDragged(e.getX(), e.getY());
            }
        });
    }

    /*
        Methodes
    */

    //Retourne l'action d'edition en cours
    public int getAction() {
        return action;
    }

    //Retourne l'item de l'action en cours
    public int getItem() {
        return item;
    }

    //Modifie l'action et l'item en cours
    public void setAction(int action, int item) {
        this.action = action;
        this.item = item;
    }
   
}
