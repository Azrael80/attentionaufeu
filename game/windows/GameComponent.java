package game.windows;

import javax.swing.*;
import java.awt.*;
import java.util.Date;

public abstract class GameComponent extends JComponent {

    private static final long serialVersionUID = 1L;
    
    public GameComponent(int x, int y) {
        super();
        setBounds(x, y, 25, 25);
    }

    public void setLocation(int x, int y, JLayeredPane pane) {
        super.setLocation(x, y);
        //pane.setLayer(this, getY());
    }

    abstract public void onLoop(Date now);

    abstract public void paint(Graphics g);
}