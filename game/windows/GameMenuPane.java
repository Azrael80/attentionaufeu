package game.windows;

import javax.swing.*;
import java.awt.*;
import game.Game;
import game.Texture;
import game.windows.buttons.FireFighterButton;
import game.windows.buttons.HomeButton;

public class GameMenuPane extends JLayeredPane {

    private static final long serialVersionUID = 1L;
    protected GamePane pane;
    protected FireFighterButton buttonBasic, buttonStrong;

    public GameMenuPane(GamePane pane) {
        super();
        this.pane = pane;
        setBounds(Game.getPane().getWidth(), 0, 205, Game.getPane().getHeight());
        setLayout(new FlowLayout());

        //Ajout du bouton, pompier normal
        buttonBasic = new FireFighterButton(19, 84, 79, 79, Texture.FireFighter00.getImage(), 0, String.format("x%s", pane.map.getBasicFireFighterCount()));
        add(buttonBasic);

        //Ajout du bouton, pompier fort
        buttonStrong = new FireFighterButton(103, 84, 79, 79, Texture.StrongFireFighter00.getImage(), 1, String.format("x%s", pane.map.getStrongFireFighterCount()));
        add(buttonStrong);

        //Ajout du bouton de retour au menu
        add(new HomeButton());
    }

    public void paintComponent(Graphics g) {
        //Actualisation du texte des boutons
        buttonBasic.setText(String.format("x%s", pane.map.getBasicFireFighterCount()));
        buttonStrong.setText(String.format("x%s", pane.map.getStrongFireFighterCount()));

        super.paintComponent(g);
        
        //Background
        g.drawImage(Texture.MenuBackground.getImage(), 0, 0, null);

        g.setColor(Color.BLACK);
        g.setFont(new Font("default", Font.BOLD, 16));
        //Affichage points
        g.drawString(String.format("Points: %d", Game.getPoints()), 23, 195);

        //Affichage nombre d'arbres
        g.drawImage(Texture.MediumTree.getImage(), 23, 210, null);
        g.drawString(String.format("x%d", Game.getTreeCount()), 50, 232);

        //Affichage nombre de feu
        g.drawImage(Texture.Fire1.getImage(), 103, 210, null);
        g.drawString(String.format("x%d", Game.getFireCount()), 130, 232);
    }
}