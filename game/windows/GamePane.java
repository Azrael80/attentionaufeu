package game.windows;

import game.Game;
import game.items.map.Map;
import javax.swing.*;
import java.awt.*;
import java.util.Date;

public class GamePane extends JLayeredPane {

    private static final long serialVersionUID = 1L;
    
    protected int width;
    protected int height;
    protected Map map;

    /*
        Initialise la panel
    */
    public GamePane() {
        super();
    }

    /*
        Methodes
    */

    //Initialise l'affichage du jeu (MAP)
    public void init(Map map) {
        this.map = map;
        this.map.init();
        this.width = this.map.getXCase() * 25;
        this.height = this.map.getYCase() * 25;

        setPreferredSize(new Dimension(width, height));
        setSize(width, height);
    }

    //Boucle principale du jeu
    public void onLoop(Date now) {
        if(map != null && map.getStarted())
        {
            map.onLoop(now);
            repaint();
            
            //Actualisation du menu du jeu
            if(Game.getGameMenuPane() != null)
                Game.getGameMenuPane().repaint();
        }
    }

	public void paintComponent(Graphics g) {
        super.paintComponent(g);
        setSize(width, height);
    }
}
