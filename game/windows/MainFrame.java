package game.windows;

import javax.swing.*;

public class MainFrame extends JFrame {

    private static final long serialVersionUID = 1L;

    public MainFrame() {
        super("Attention au feu !");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(null);
        setSize(getSize().width, getSize().height + getInsets().top + getInsets().bottom);
    }

    /*
        Ouvre le menu
    */
    public void openMenu(JLayeredPane pane) {
        //Mise à jour de la taille afin d'avoir le contenu complet
        setVisible(true);
        setSize(pane.getWidth(), pane.getHeight());
        setSize(getSize().width, getSize().height + getInsets().top + getInsets().bottom);
        add(pane);
    }

    /*
        Ouvre le jeu
    */
    public void open(JLayeredPane pane) {
        //Mise à jour de la taille afin d'avoir le contenu complet
        setSize(pane.getWidth(), pane.getHeight());
        setSize(getSize().width + 205, getSize().height + getInsets().top + getInsets().bottom);
        add(pane);
    }
}
