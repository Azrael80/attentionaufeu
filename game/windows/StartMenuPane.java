package game.windows;

import javax.swing.*;
import game.Game;
import game.Texture;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class StartMenuPane extends JLayeredPane {

    private static final long serialVersionUID = 1L;

    public StartMenuPane() {
        super();
        setSize(500, 250);

        /*
            Action souris, clique
        */
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if(e.getX() > 93 && e.getX() <= 391) {
                    if(e.getY() >= 32 && e.getY() <= 74) {
                        //Niveau aléatoire choisit
                        Game.startRandomMap();
                    }
                    else if(e.getY() >= 80 && e.getY() <= 123) {
                        //Chargement à partir de fichier
                        Game.loadMapLevel();
                    }
                    else if(e.getY() >= 129 && e.getY() <= 171)
                    {
                        //Editeur de niveau
                        Game.startEditorMap();
                    }
                }
            }
        });

        /*
            Action souris, déplacement
        */
        addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                if(e.getX() > 93 && e.getX() <= 391 && (e.getY() >= 32 && e.getY() <= 74 || e.getY() >= 80 && e.getY() <= 123 || e.getY() >= 129 && e.getY() <= 171))
                    Game.changeCursor(new Cursor(Cursor.HAND_CURSOR));
                else
                    Game.changeCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }
        });

    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(Texture.Menu.getImage(), 0, 0, null);
    }

}