package game.windows.buttons;

import game.Game;
import game.Texture;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class FireFighterButton extends MenuButton {

    private static final long serialVersionUID = 1L;

    private Image fireFighterImage;
    private int fireFighter;
    private String text;
    
    public FireFighterButton(int x, int y, int w, int h, Image img, int f, String t) {
        super(x, y, w, h);
        fireFighterImage = img;
        fireFighter = f;
        text = t;

        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.HAND_CURSOR));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }

            //Passage de l'action sur dessin de l'herbe
            @Override
            public void mousePressed(MouseEvent e) {
                Game.setPlaceFireFighter(fireFighter);
            }
        });
    }

    //Change le texte dans le boutton
    public void setText(String text) {
        this.text = text;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.drawImage(Texture.FireFighterButton.getImage(), 0, 0, null);
        g.drawImage(fireFighterImage, 26, 5, null);
        g.setColor(Color.BLACK);
        g.setFont(new Font("default", Font.BOLD, 16));
        g.drawString(text, 25, 50);
    }

}