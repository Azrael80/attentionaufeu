package game.windows.buttons;

import game.Game;
import game.Texture;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class HomeButton extends MenuButton {

    private static final long serialVersionUID = 1L;
    
    public HomeButton() {
        super(61, 265, 79, 79);

        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.HAND_CURSOR));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                Game.changeCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }

            //Retourne sur le menu
            @Override
            public void mousePressed(MouseEvent e) {
                Game.openStartMenu();
            }
        });
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.drawImage(Texture.HomeButton.getImage(), 0, 0, null);
    }

}