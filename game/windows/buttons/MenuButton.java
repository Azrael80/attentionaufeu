package game.windows.buttons;

import javax.swing.JPanel;
import java.awt.*;

public class MenuButton extends JPanel {

    private static final long serialVersionUID = 1L;

    protected int x, y, w, h;

    public MenuButton(int x, int y, int w, int h) {
        super();
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        setBounds(x, y, w, h);
    }
    
}