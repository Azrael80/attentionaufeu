package game.windows.popup;

/*
    Classe affichage résultat de fin de partie
    avec nombre de points, pompiers morts et arbres sauvés
*/

import javax.swing.*;
import java.awt.*;

public class EndPopup extends Popup {

    private int trees, points, deadFirefighter;
    int x, y, width, height;

    public EndPopup(int trees, int points, int deadFirefighter, int x, int y, int width, int height, JLayeredPane pane, Image bg) {
        super(x, y, width, height, pane, bg);
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;

        this.trees = trees;
        this.points = points;
        this.deadFirefighter = deadFirefighter;
        
    }

    private static final long serialVersionUID = 1L;

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        setBounds(x, y, width, height);

        //Affichage des scores
        g.setColor(Color.BLACK);
        g.setFont(new Font("default", Font.BOLD, 16));
        g.drawString(String.format("Points: %d", points), 130, 100);
        g.drawString(String.format("Arbres sauvés: %d", trees), 130, 117);
        g.drawString(String.format("Pompiers morts: %d", deadFirefighter), 130, 134);
    }
    
}