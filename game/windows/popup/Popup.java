package game.windows.popup;

import javax.swing.*;
import java.awt.*;

abstract public class Popup extends JComponent {

    private static final long serialVersionUID = 1L;
    protected Image background;
    
    public Popup(int x, int y, int width, int height, JLayeredPane pane, Image bg) {
        super();
        background = bg;
        pane.add(this);
        pane.setLayer(this, 5000);
        setBounds(x, y, width, height);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        //Dessin du fond
        g.drawImage(background, 0, 0, null);

    }

}